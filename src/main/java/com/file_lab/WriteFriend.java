/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.file_lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.jfr.Frequency;


/**
 *
 * @author acer
 */
public class WriteFriend {
   public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("JM", 21, "0123456789");
            Friend friend2 = new Friend("AP", 4, "0123455678");
            Friend friend3 = new Friend("JMA", 22, "0123445678");
            File file = new File("friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos  = new ObjectOutputStream(fos);
            oos.writeObject(friend1);
            oos.writeObject(friend2);
            oos.writeObject(friend3);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}


